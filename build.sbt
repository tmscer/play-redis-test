name := """play-scala-starter-example"""

version := "1.0-SNAPSHOT"

lazy val rediscache = RootProject(file("rediscache"))
lazy val root = (project in
		file(".")).enablePlugins(PlayScala).dependsOn(rediscache)

/* resolvers += Resolver.sonatypeRepo("snapshots") */

scalaVersion := "2.11.12"

crossScalaVersions := Seq("2.11.12", "2.12.6")

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
libraryDependencies += "com.h2database" % "h2" % "1.4.197"


libraryDependencies ++= Seq(
  play.sbt.PlayImport.cacheApi,
  "com.typesafe.play" %% "play-cache" % "2.6.15" % Provided,
  "com.github.Ma27" %% "rediscala" % "1.8.3",
  "org.specs2" %% "specs2-core" % "4.2.0" % Test,
  "org.specs2" %% "specs2-mock" % "4.2.0" % Test,
  "com.typesafe.play" %% "play-specs2" % "2.6.15" % Test
)

