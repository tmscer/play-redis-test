package controllers

import javax.inject._
import play.api.mvc._
import services.Counter
import play.api.cache.redis.CacheAsyncApi

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

/**
 * This controller demonstrates how to use dependency injection to
 * bind a component into a controller class. The class creates an
 * `Action` that shows an incrementing count to users. The [[Counter]]
 * object is injected by the Guice dependency injection system.
 */
@Singleton
class CountController @Inject() (cache: CacheAsyncApi, cc: ControllerComponents) extends AbstractController(cc) {

  var counter: Long = 0

  /**
   * Create an action that responds with the [[Counter]]'s current
   * count. The result is plain text. This `Action` is mapped to
   * `GET /count` requests by an entry in the `routes` config file.
   */
  def count = Action {
    val next: Long = Await.result(cache.increment("xxx", 2), Duration.Inf).longValue()
    this.counter = next
    Ok(next.toString)
  }

}
